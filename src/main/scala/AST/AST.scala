package AST

object Priority {
  val binary = Map("lambda" -> 1,
    "or" -> 2,
    "and" -> 3,
    "is" -> 8, "<" -> 8, ">" -> 8, ">=" -> 8, "<=" -> 8, "==" -> 8, "!=" -> 8,
    "+" -> 9, "-" -> 9,
    "*" -> 10, "/" -> 10, "%" -> 10)

  val unary = Map("not" -> 4,
    "+" -> 12, "-" -> 12)
}

// sealed trait Node would be also OK
sealed abstract class Node {
  def toStr: String

  val indent = " " * 4
}

case class IntNum(value: Int) extends Node {
  override def toStr = value.toString
}

case class FloatNum(value: Double) extends Node {
  override def toStr = value.toString
}

case class StringConst(value: String) extends Node {
  override def toStr = value
}

case object TrueConst extends Node {
  override def toStr = "True"
}

case object FalseConst extends Node {
  override def toStr = "False"
}

case class Variable(name: String) extends Node {
  override def toStr = name
}

case class Unary(op: String, expr: Node) extends Node {

  override def toStr = {
    val modifiedString = expr match {
      case BinExpr(operator, _, _) if Priority.binary(operator) <= Priority.unary(op) => s"(${expr.toStr})"
      case Unary(operator, _) if Priority.unary(operator) <= Priority.unary(op) => s"(${expr.toStr})"
      case _ => expr.toStr
    }
    s"op $modifiedString"
  }

}

case class BinExpr(op: String, left: Node, right: Node) extends Node {

  override def toStr = {
    val leftStr = left match {
      case BinExpr(operator, _, _) if Priority.binary(operator) < Priority.binary(op) => s"(${left.toStr})"
      case Unary(operator, _) if Priority.unary(operator) < Priority.binary(op) => s"(${left.toStr}"
      case _ => left.toStr
    }
    val rightStr = right match {
      case BinExpr(operator, _, _) if Priority.binary(operator) < Priority.binary(op) => s"(${right.toStr})"
      case Unary(operator, _) if Priority.unary(operator) < Priority.binary(op) => s"(${right.toStr}"
      case _ => right.toStr
    }
    s"$leftStr $op $rightStr"
  }
}

case class IfElseExpr(cond: Node, left: Node, right: Node) extends Node {
  override def toStr = left.toStr + " if " + cond.toStr + " else " + right.toStr
}

case class Assignment(left: Node, right: Node) extends Node {
  override def toStr = left.toStr + " = " + right.toStr
}

case class Subscription(expr: Node, sub: Node) extends Node {
  override def toStr = expr.toStr + "[" + sub.toStr + "]"
}

case class KeyDatum(key: Node, value: Node) extends Node {
  override def toStr = key.toStr + ": " + value.toStr
}

case class GetAttr(expr: Node, attr: String) extends Node {
  override def toStr = expr.toStr + "." + attr
}

case class IfInstr(cond: Node, left: Node) extends Node {
  override def toStr = {
    s"if ${cond.toStr} :\n ${left.toStr.replaceAll("(?m)^", indent)}"
  }
}

case class ElifInstr(cond: Node, left: Node) extends Node {
  override def toStr = {
    s"elif ${cond.toStr} :\n ${left.toStr.replaceAll("(?m)^", indent)}"
  }
}
case class IfElifElseInstr(ifInstr: IfInstr, elifInstr: List[ElifInstr], elseInstr: Option[Node]) extends Node {
  override def toStr: String = {
    val base = s"${ifInstr.toStr}\n${elifInstr.map(_.toStr).mkString("\n")}\n"
    val elsePart = elseInstr match {
      case Some(node) => node.toStr.replaceAll("(?m)^", indent)
      case _ => ""
    }
    base + elsePart
  }
}

case class WhileInstr(cond: Node, body: Node) extends Node {
  override def toStr = {
    "while " + cond.toStr + ":\n" + body.toStr.replaceAll("(?m)^", indent)
  }
}

case class InputInstr() extends Node {
  override def toStr = "input()"
}

case class ReturnInstr(expr: Node) extends Node {
  override def toStr = "return " + expr.toStr
}

case class PrintInstr(expr: Node) extends Node {
  override def toStr = "print " + expr.toStr
}

case class FunCall(name: Node, args_list: Node) extends Node {

  override def toStr = {
    args_list match {
      case NodeList(list) => name.toStr + "(" + list.map(_.toStr).mkString("", ",", "") + ")"
      case _ => name.toStr + "(" + args_list.toStr + ")"
    }
  }
}

case class FunDef(name: String, formal_args: Node, body: Node) extends Node {
  override def toStr = {
    var str = "\ndef " + name + "(" + formal_args.toStr + "):\n"
    str += body.toStr.replaceAll("(?m)^", indent) + "\n"
    str
  }
}

case class LambdaDef(formal_args: Node, body: Node) extends Node {
  override def toStr = "lambda " + formal_args.toStr + ": " + body.toStr
}

case class ClassDef(name: String, inherit_list: Node, suite: Node) extends Node {
  override def toStr = {
    val str = "\nclass " + name
    val inheritStr = inherit_list match {
      case NodeList(x) if x.nonEmpty => "(" + x.map(_.toStr).mkString("", ",", "") + ")"
      case _ => ""
    }
    val suiteStr = ":\n" + suite.toStr.replaceAll("(?m)^", indent)
    s"$str $inheritStr $suiteStr"
  }
}

case class NodeList(list: List[Node]) extends Node {
  override def toStr = {
    list.map(_.toStr).mkString("", "\n", "")
  }
}

case class KeyDatumList(set: Set[KeyDatum]) extends Node {
  override def toStr = set.map(_.toStr).mkString("{", ",", "}")
}

case class IdList(list: List[Variable]) extends Node {
  override def toStr = list.map(_.toStr).mkString("", ",", "")
}

case class ElemList(list: List[Node]) extends Node {
  override def toStr = list.map(_.toStr).mkString("[", ",", "]")
}

case class ElemTuple(list: List[Node]) extends Node {
  override def toStr = if (list.isEmpty) "()"
  else if (list.length == 1) "(" + list.head.toStr + ",)"
  else list.map(_.toStr).mkString("(", ",", ")")
}


case object EmptyNode extends Node {
  override def toStr: String = ""
}

        