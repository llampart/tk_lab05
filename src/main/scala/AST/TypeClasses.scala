package AST
import AST.BinaryExpressionUtils._
import scala.language.implicitConversions

object TypeClasses {

  /**
    * This class provides extension methods for [[AST.Node]] class
    * @param lhs
    */
  implicit class LookUpExtension(private val lhs: Node) extends AnyVal {



    def simplify: Node = lhs match {
      case BinExpr("-", l@BinExpr("+", _, _), right) =>
        new LookUpExtension(l).removeElem(right)
      case _ => lhs
    }

    private def removeNode(arg: Node): Node = arg match {
      case b: BinExpr => removeBinExpr(b)
      case _ => removeElem(arg)
    }

    private def removeBinExpr(arg: BinExpr): Node = lhs match {
      case BinExpr("+", l: BinExpr, r) if l === arg => r
      case BinExpr("+", l, r: BinExpr) if r === arg => l
      case Unary("-", l: BinExpr) if l === arg => NodeList(Nil)
      case _ => lhs
    }

    private def removeElem(arg: Node): Node = lhs match {
      case BinExpr("+", l, r) if l == arg => r
      case BinExpr("+", l, r) if r == arg => l
      case Unary("-", l) if l == arg => NodeList(Nil)
      case _ => lhs
    }


  }

  trait ExtendedNumeric[T] extends Numeric[T] {
    def divide(x: T, y: T): T

    def raise(x: T, y: T): T

    class ExtendedOps(lhs: T) extends Ops(lhs) {
      def /(rhs: T) = divide(lhs, rhs)

      def **(rhs: T) = raise(lhs, rhs)
    }

    implicit def mkExtendedOps(lhs: T): ExtendedOps = new ExtendedOps(lhs)
  }

  object ExtendedNumeric {
    def unapply(arg: Node): Boolean = arg match {
      case _: IntNum => true
      case _: FloatNum => true
      case _ => false
    }
  }

  object ExtNumWithEvidence {
    def unapply(arg: Node): Option[ExtendedNumeric[_ <: Node]] = arg match {
      case _: IntNum => Some(IntNumericEvidence)
      case _: FloatNum => Some(FloatNumericEvidence)
      case _ => None
    }
  }

  object One {
    def unapply(arg: Node): Boolean = arg match {
      case IntNum(1) => true
      case FloatNum(1.0) => true
      case ElemList(head :: Nil) => true
      case ElemTuple(head :: Nil) => true
      case _ => false
    }
  }

  object Zero {
    def unapply(arg: Node): Boolean = arg match {
      case IntNum(0) => true
      case FloatNum(0.0) => true
      case ElemList(Nil) => true
      case ElemTuple(Nil) => true
      case _ => false
    }
  }

  trait Monoid[T] {

    def add(x: T, y: T): T

    def zero: T

    class Ops(lhs: T) {

      def +(rhs: T): T = add(lhs, rhs)

    }

    implicit def mkOps(lhs: T): Ops = new Ops(lhs)
  }

  object Monoid {
    def unapply(arg: Node): Boolean = arg match {
      case t: ElemTuple => true
      case l: ElemList => true
      case _ => false
    }
  }

  implicit object IntNumericEvidence extends ExtendedNumeric[IntNum] {
    override def plus(x: IntNum, y: IntNum): IntNum = IntNum(x.value + y.value)

    override def toDouble(x: IntNum): Double = x.value.toDouble

    override def toFloat(x: IntNum): Float = x.value.toFloat

    override def toInt(x: IntNum): Int = x.value

    override def negate(x: IntNum): IntNum = IntNum(-x.value)

    override def fromInt(x: Int): IntNum = IntNum(x)

    override def toLong(x: IntNum): Long = x.value.toLong

    override def times(x: IntNum, y: IntNum): IntNum = IntNum(x.value * y.value)

    override def minus(x: IntNum, y: IntNum): IntNum = IntNum(x.value - y.value)

    override def compare(x: IntNum, y: IntNum): Int = x.value - y.value

    override def divide(x: IntNum, y: IntNum): IntNum = IntNum(x.value / y.value)

    override def raise(x: IntNum, y: IntNum): IntNum = power(x, y)
  }

  implicit object FloatNumericEvidence extends ExtendedNumeric[FloatNum] {
    override def plus(x: FloatNum, y: FloatNum): FloatNum = FloatNum(x.value + y.value)

    override def toDouble(x: FloatNum): Double = x.value

    override def toFloat(x: FloatNum): Float = x.value.toFloat

    override def toInt(x: FloatNum): Int = x.value.toInt

    override def negate(x: FloatNum): FloatNum = FloatNum(-x.value)

    override def fromInt(x: Int): FloatNum = FloatNum(x.toDouble)

    override def toLong(x: FloatNum): Long = x.value.toLong

    override def times(x: FloatNum, y: FloatNum): FloatNum = FloatNum(x.value * y.value)

    override def minus(x: FloatNum, y: FloatNum): FloatNum = plus(x, negate(y))

    override def compare(x: FloatNum, y: FloatNum): Int = toInt(x) - toInt(y)

    override def divide(x: FloatNum, y: FloatNum): FloatNum = FloatNum(x.value / y.value)

    override def raise(x: FloatNum, y: FloatNum): FloatNum = FloatNum(Math.pow(x.value, y.value))
  }


  def power(x: IntNum, y: IntNum): IntNum = {
    def tailrec(acc: IntNum, x: IntNum, counter: Int): IntNum =
      if (counter == 0) {
        acc
      } else {
        tailrec(IntNum(acc.value * x.value), x, counter - 1)
      }
    tailrec(IntNum(1), x, y.value)
  }


  class TupleExtension(private val lhs: ElemTuple) extends AnyVal {

    def +(rhs: ElemTuple) = ElemTuple(lhs.list ++ rhs.list)

    def add(rhs: ElemTuple) = ElemTuple(lhs.list ++ rhs.list)

  }

  class ListExtension(private val lhs: ElemList) extends AnyVal {

    def +(rhs: ElemList) = ElemList(lhs.list ++ rhs.list)

    def add(rhs: ElemList) = ElemList(lhs.list ++ rhs.list)

  }

  implicit def getTupleExtension(lhs: ElemTuple): TupleExtension = new TupleExtension(lhs)

  implicit def getListExtension(rhs: ElemList): ListExtension = new ListExtension(rhs)


}
