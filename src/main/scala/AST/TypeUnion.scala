package AST
//via http://milessabin.com/blog/2011/06/09/scala-union-types-curry-howard/
object TypeUnion {

  type ¬[A] = A => Nothing

  type ∨[T, U] = ¬[¬[T] with ¬[U]]

  type ¬¬[A] = ¬[¬[A]]

  //to avoid conflicts with |, and || operators
  type |||[T, U] = { type Type[X] = ¬¬[X] <:< (T ∨ U) }


}
