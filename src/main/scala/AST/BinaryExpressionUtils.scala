package AST

object BinaryExpressionUtils {

  implicit class RichBinaryExpression(private val lhs: BinExpr) extends AnyVal {

    def ===(rhs: BinExpr): Boolean = (lhs == rhs) || {
      (lhs, rhs) match {
        case (BinExpr(a, ll: BinExpr, rl: BinExpr), BinExpr(b, lr: BinExpr, rr: BinExpr)) if a == b && isCorrect(a) =>
          (ll === lr && rl === rr) || (ll === rr && rl === lr)
        case (BinExpr(a, ll: BinExpr, rl), BinExpr(b, lr: BinExpr, rr)) if a == b =>
          rl == rr && ll === lr
        case (BinExpr(a, ll, rl: BinExpr), BinExpr(b, lr: BinExpr, rr)) if a == b && isCorrect(a) =>
          ll == rr && rl === lr
        case (BinExpr(a, ll: BinExpr, rl), BinExpr(b, lr, rr: BinExpr)) if a == b && isCorrect(a) =>
          rl == lr && ll === rr
        case (BinExpr(a, ll, rl: BinExpr), BinExpr(b, lr, rr: BinExpr)) if a == b =>
          ll == lr && rl === rr
        case (BinExpr(a, ll, rl), BinExpr(b, lr, rr)) if a == b =>
          ll == lr && rl == rr || (ll == rr && rl == lr && isCorrect(a))
        case _ => false
      }
    }

    private def isCorrect(operator: String): Boolean = operator match {
      case "+" | "*" | "and" | "or" => true
      case _ => false
    }

  }

}
