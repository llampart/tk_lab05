package simplifier

import AST.BinaryExpressionUtils._
import AST.TypeClasses._
import AST._

/**
  * General strategy explanation:
  * Upon receiving expression to simplify there are several things happening every time:
  * 1. Expression "children" are optimised
  * 2. Expression itself is optimised
  * 3. If structure of the expression changes due to optimisation(*), it's performed again
  * *By change of the structure i mean change performed at the level of root expression
  */
object ExpressionSimplifier {


  /**
    * Purpose of this method is to evaluate expression during optimisation
    * @param op operator of the expression
    * @param a left side of the expression
    * @param b right side of the expression
    * @tparam T type of elements of the expression, it has to conform to ExtendedNumeric contract
    * @return Evaluated expression
    */
  private def eval[T <: Node : ExtendedNumeric](op: String)(a: T, b: T): Node = {
    val ev = implicitly[ExtendedNumeric[T]]
    import ev.{mkExtendedOps, mkOrderingOps}
    op match {
      case "+" => a + b
      case "-" => a - b
      case "*" => a * b
      case "/" => a / b
      case "**" => a ** b
      case "<" if a < b => TrueConst
      case "<" => FalseConst
      case "<=" if a <= b => TrueConst
      case "<=" => FalseConst
      case ">" if a > b => TrueConst
      case ">" => FalseConst
      case ">=" if a >= b => TrueConst
      case ">=" => FalseConst
    }
  }

  //akka-style approach
  /**
    * This function tries to optimise (evaluate) expression of Monoid or ExtendedNumeric type.
    * This function has to main purposes:
    * a) Determine type of the expression elements at compile time (needed for eval method)
    * b) Provide casting of types if necessary
    */
  lazy val simplifyConstants: PartialFunction[BinExpr, Node] = {
    case BinExpr(op, l: IntNum, r: IntNum) => eval(op)(l, r)
    case BinExpr(op, l: FloatNum, r: FloatNum) => eval(op)(l, r)
    case BinExpr(op, l: IntNum, r: FloatNum) => eval(op)(FloatNum(l.value), r)
    case BinExpr(op, l: FloatNum, r: IntNum) => eval(op)(l, FloatNum(r.value))
    case BinExpr("+", l: ElemTuple, r: ElemTuple) => l + r
    case BinExpr("+", l: ElemList, r: ElemList) => l + r
  }


  /**
    * This function tries to optimise expression consisting of at least one Monoid or ExtendedNumeric element
    * It delegates boolean optimisations to it's own function. One, Zero extractors allow for using the same code for lists, tuples and numbers
    * Some power laws are recognized here
    */
  lazy val simplifyConstantsAdvanced: PartialFunction[BinExpr, Node] = {
    case BinExpr(("*" | "/"), nl@Zero(), r) => nl
    case BinExpr("*", l, nr@Zero()) => nr
    case BinExpr(("*" | "/"), l, One()) => Simplifier.simplify(l)
    case BinExpr("*", One(), r) => Simplifier.simplify(r)
    case BinExpr("/", One(), BinExpr("/", n, elem)) => Simplifier.simplify(elem)
    case BinExpr(("+" | "-"), l, Zero())  => Simplifier.simplify(l)
    case BinExpr("+", Zero(), r) => Simplifier.simplify(r)
    case BinExpr("-", Zero(), r) => simplifyUnary(Unary("-", r))
    case expr@BinExpr("or" | "and", FalseConst | TrueConst, _) => simplyfyBoolean(expr)
    case expr@BinExpr("or" | "and", _, FalseConst | TrueConst) => simplyfyBoolean(expr)
    case expr@BinExpr("+", l: BinExpr, mr@Monoid()) => simplifyBinExprRecursive(l) match {
      case ml@Monoid() => simplifyConstants(BinExpr("+", ml, mr))
      case n: Node => expr.copy(left = n)
    }
    case BinExpr("**", l, nr@Zero()) => IntNum(1)
    case BinExpr("**", l, One()) => Simplifier.simplify(l)
    case expr@BinExpr("**", l, nr@ExtendedNumeric()) => Simplifier.simplify(l) match {
      case nl@ExtendedNumeric() => simplifyConstants(BinExpr("**", nl, nr))
      case n => expr.copy(left = n)
    }
    case expr@BinExpr(op, l, nr@ExtendedNumeric()) => Simplifier.simplify(l) match {
      case nl@ExtendedNumeric() => simplifyConstants(BinExpr(op, nl, nr))
      case n: Node => expr.copy(left = n)
    }
    case expr@BinExpr(op, nl@ExtendedNumeric(), r) => Simplifier.simplify(r) match {
      case nr@ExtendedNumeric() => simplifyConstants(BinExpr(op, nl, nr))
      case n: Node => expr.copy(right = n)
    }
  }

  /**
    * This function simplifies boolean expressions
    */
  lazy val simplyfyBoolean: PartialFunction[BinExpr, Node] = {
    case BinExpr("or", FalseConst, l) => l
    case BinExpr("or", r, FalseConst) => r
    case BinExpr("or", TrueConst, l) => TrueConst
    case BinExpr("or", r, TrueConst) => TrueConst
    case BinExpr("and", FalseConst, l) => FalseConst
    case BinExpr("and", r, FalseConst) => FalseConst
    case BinExpr("and", TrueConst, l) => l
    case BinExpr("and", r, TrueConst) => r
    case b => b
  }

  /**
    * Top level method for optimisation following the pattern fo the entire module
    */
  lazy val simplifyBinExprRecursive: PartialFunction[BinExpr, Node] = {
    case expr@BinExpr(op, l, r) =>
      val simplified = BinExpr(op, Simplifier.simplify(l), Simplifier.simplify(r))
      simplifyBinExpr(simplified)
  }

  /**
    * Most important function recognizing most of the laws etc.
    * In case of receiving binaryExpression it tries to evaluate it if it is impossible it performs more advanced operations
    * Division, power and subtraction laws are recognized here.
    * Take a note that sub-expressions are not simplified, it is assumed that it was already done
    */
  lazy val simplifyBinExpr: PartialFunction[BinExpr, Node] = simplifyConstants orElse simplifyConstantsAdvanced orElse {
    case expr@BinExpr("/", l: BinExpr, r: BinExpr) if l === r => IntNum(1)
    case expr@BinExpr("/", left, right) if left == right => IntNum(1)
    case expr@BinExpr("or" | "and", l: BinExpr, r: BinExpr) if l === r => l
    case expr@BinExpr("or" | "and", left, right) if left == right => left
    case expr@BinExpr("==" | ">=" | "<=", l: BinExpr, r: BinExpr) if l === r => TrueConst
    case expr@BinExpr("!=" | ">" | "<", l: BinExpr, r: BinExpr) if l === r => FalseConst
    case expr@BinExpr("==" | ">=" | "<=", left, right) if left == right => TrueConst
    case expr@BinExpr("!=" | ">" | "<", left, right) if left == right => FalseConst
    case expr@BinExpr("*", l, BinExpr("/", n@ExtNumWithEvidence(numeric), r)) if n == numeric.one =>
      simplifyBinExprRecursive(BinExpr("/", l, r))
    case BinExpr("*", BinExpr("**", ll, lr), BinExpr("**", rl, rr)) if ll == rl =>
      BinExpr("**", ll, simplifyBinExprRecursive(BinExpr("+", lr, rr)))
    case BinExpr("**", BinExpr("**", base, x), y) =>
      BinExpr("**", Simplifier.simplify(base), simplifyBinExprRecursive(BinExpr("*", x, y)))
    case BinExpr(op@("+" | "-"), l@BinExpr("*", _, _), r@BinExpr("*", _, _)) => simplifyDistributiveProperty(op, l, r)
    case expr@BinExpr(op@("+" | "-"), l@BinExpr("*", ll, lr), r) =>
      if (ll == r) {
        simplifyBinExprRecursive(BinExpr("*", ll, BinExpr(op, lr, IntNum(1))))
      } else if (lr == r) {
        simplifyBinExprRecursive(BinExpr("*", BinExpr(op, ll, IntNum(1)), lr))
      } else {
        expr
      }
    case expr@BinExpr(op@("+" | "-"), l, r@BinExpr("*", rl, rr)) =>
      if (rl == l) {
        simplifyBinExprRecursive(BinExpr("*", rl, BinExpr(op, rr, IntNum(1))))
      } else if (rr == r) {
        simplifyBinExprRecursive(BinExpr("*", BinExpr(op, rl, IntNum(1)), rr))
      } else {
        expr
      }
    case expr@BinExpr("-", l: BinExpr, r: BinExpr) if l === r => IntNum(0)
    case expr@BinExpr("-", left, right) if left == right => IntNum(0)
    case expr@BinExpr("-", left, right) => expr.simplify
    case b => b
  }


  /**
    * Method name describe its behaviour, it struggles to recognize and simplify some basic distributive properties of multiplication
    * @param op "+" or "-" operator
    * @param leftExpr "self describing"
    * @param rightExpr "self describing"
    * @return Simplified expression
    */
  def simplifyDistributiveProperty(op: String, leftExpr: BinExpr, rightExpr: BinExpr): Node = {
    val sL@BinExpr(_,ll,lr) = simplifyBinExprRecursive(leftExpr)
    val sR@BinExpr(_,rl,rr) = simplifyBinExprRecursive(rightExpr)
    val rawExpr = (ll, lr, rl, rr) match {
      case (ll: BinExpr, lr: BinExpr, rl: BinExpr, rr: BinExpr) =>
        if (ll === rl) {
          BinExpr("*", ll, BinExpr(op, lr, rr))
        } else if (ll === rr) {
          BinExpr("*", ll, BinExpr(op, lr, rl))
        } else if (lr === rr) {
          BinExpr("*", BinExpr(op, ll, rl), lr)
        } else if (lr === rl) {
          BinExpr("*", BinExpr(op, ll, rr), lr)
        } else {
          BinExpr(op, sL, sR)
        }
      case _ =>
        if (ll == rl) {
          BinExpr("*", ll, BinExpr(op, lr, rr))
        } else if (ll == rr) {
          BinExpr("*", ll, BinExpr(op, lr, rl))
        } else if (lr == rr) {
          BinExpr("*", BinExpr(op, ll, rl), lr)
        } else if (lr == rl) {
          BinExpr("*", BinExpr(op, ll, rr), lr)
        } else {
          BinExpr(op, sL, sR)
        }
    }
    if (rawExpr.op == op) {
      rawExpr
    } else {
      simplifyBinExprRecursive(rawExpr)
    }
  }

  /**
    * This function tries to simplify unary expression by applying some basic laws (comparison negation etc.)
    */
  lazy val simplifyUnary: PartialFunction[Unary, Node] = {
    case Unary("not", FalseConst) => TrueConst
    case Unary("not", TrueConst) => FalseConst
    case Unary("-", elem@ExtNumWithEvidence(numeric)) => simplifyConstants(BinExpr("-", numeric.zero, elem))
    case Unary("+", elem) => Simplifier.simplify(elem)
    case Unary("not", expr@BinExpr("==" | "!=" | "<=" |">=" | ">" | "<", _, _)) => negateComparison(expr)
    case Unary(op@("-"|"not"), elem) => Simplifier.simplify(elem) match {
      case elem@ExtendedNumeric() => simplifyUnary(Unary(op, elem))
      case Unary(`op`, nestedElem) => Simplifier.simplify(nestedElem)
      case simplifiedElem => Unary(op, simplifiedElem)
    }
  }

  /**
    * Self describing
    * @param expr expression to simplify
    * @return simplified expression
    */
  def negateComparison(expr: BinExpr): BinExpr = expr match {
    case e@BinExpr("==", _, _) => e.copy(op = "!=")
    case e@BinExpr("!=", _, _) => e.copy(op = "==")
    case e@BinExpr(">=", _, _) => e.copy(op = "<")
    case e@BinExpr("<=", _, _) => e.copy(op = ">")
    case e@BinExpr(">", _, _) => e.copy(op = "<=")
    case e@BinExpr("<", _, _) => e.copy(op = ">=")
  }

}
