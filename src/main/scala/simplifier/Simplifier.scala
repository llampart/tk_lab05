package simplifier

import AST._

// to implement
// avoid one huge match of cases
// take into account non-greedy strategies to resolve cases with power laws
object Simplifier {

  /**
    * "Top" simplifing function, invoked on Nodes
    */
  val simplify: PartialFunction[Node, Node] = {
    case b: BinExpr => ExpressionSimplifier.simplifyBinExprRecursive(b)
    case u: Unary => ExpressionSimplifier.simplifyUnary(u)
    case d: KeyDatumList => simplifyDict(d)
    case iee: IfElseExpr => simplifyIfExpr(iee)
    case iei: IfElifElseInstr => simplifyIfElifElseInstr(iei)
    case Assignment(left, right) if left == right => NodeList(Nil)
    case Assignment(left, right) => Assignment(left, simplify(right))
    case w: WhileInstr => simplifyLoop(w)
    case NodeList(n :: Nil) => simplify(n)
    case NodeList(NodeList(Nil) :: Nil) => NodeList(Nil)
    case l: NodeList => simplifyNodeList(l)
    case f@FunDef(_, _, body) =>
      f.copy(body = simplify(body))
    case PrintInstr(expr) => PrintInstr(simplify(expr))
    case ReturnInstr(expr) => ReturnInstr(simplify(expr))
    case l@LambdaDef(_, body) => l.copy(body = simplify(body))
    case n => n
  }

  /**
    * This method tries to simplify node list, looking for dead assignments
    * @param list  node list to simplify
    * @return simplified argument
    */
  def simplifyNodeList(list: NodeList): Node = {
    val simplifiedList = NodeList(list.list.map(simplify))
    simplifiedList match {
      case n@NodeList(Nil) => n
      case NodeList(ls) =>
        val partialList = (ls :+ NodeList(Nil)).sliding(2, 1)
        NodeList(partialList.filter {
          case Assignment(l, _) :: Assignment(r, _) :: Nil if l == r => false
          case _ => true
        }.flatMap(_.headOption).toList)
      case `list` => simplifiedList

    }
  }

  /**
    * One important note, function KeyDatum.unapply has a following signature:
    * KeyDatum => Option((Node, Node)), therefore usage of flatMap is necessary to unpack pairs
    * @param dict
    * @return
    */
  def simplifyDict(dict: KeyDatumList): Node = {
    val keyDatumSet = dict.set.iterator.flatMap(KeyDatum.unapply).foldLeft(Map.empty[Node, Node])(_ + _)
    KeyDatumList(keyDatumSet.iterator.map(KeyDatum.tupled).toSet)
  }

  def simplifyElifInstr(intr: ElifInstr): ElifInstr = ElifInstr(simplify(intr.cond), simplify(intr.left))

  def simplifyIfInstr(instr: IfInstr): IfInstr = IfInstr(simplify(instr.cond), simplify(instr.left))

  /**
    * This method is quite complex because it is quite general. It can optimise instruction consisting of arbitrary number of instructions.
    * It splits instruction into 3 main parts:
    * 1. Simplified if instruction
    * 2. Simplified elif instructions with nontrivial condition
    * 3. Simplified elif instructions (else is consider elif instruction with it's condition being True constant) with it's condition being True
    * After splitting instructions all possible simplification is performed
    * @param instr
    * @return
    */
  def simplifyIfElifElseInstr(instr: IfElifElseInstr): Node = {
    val simplifiedIf = simplifyIfInstr(instr.ifInstr)
    val restCases = instr.elifInstr.toStream.map(simplifyElifInstr) ++ instr.elseInstr.map(ElifInstr(TrueConst, _))
    val unsureConditions = restCases.takeWhile(_.cond != TrueConst).filter(_.cond != FalseConst)
    val (_, unreachable) = restCases.splitAt(unsureConditions.length)
    if (simplifiedIf.cond == TrueConst) {
      simplifiedIf.left match {
        case NodeList((n: NodeList) :: Nil) => n
        case n => n
      }
    } else if (simplifiedIf.cond == FalseConst) {
      if (restCases.isEmpty) {
        NodeList(Nil)
      } else if (restCases.length == 1 && unsureConditions.isEmpty) {
        simplifyIfElifElseInstr(IfElifElseInstr(IfInstr(restCases.head.cond, restCases.head.left), Nil, None))
      } else if (unsureConditions.isEmpty) {
        unreachable.head.left
      } else {
        val head #:: tail = unsureConditions
        IfElifElseInstr(IfInstr(head.cond, head.left), tail.toList, unreachable.headOption.map(_.left).map(simplify))
      }
    } else {
      IfElifElseInstr(simplifiedIf, unsureConditions.toList, unreachable.headOption.map(_.left).map(simplify))
    }
  }

  def simplifyIfExpr(expr: IfElseExpr): Node = IfElseExpr(simplify(expr.cond), simplify(expr.left), simplify(expr.right)) match {
    case IfElseExpr(TrueConst, left, _) => left
    case IfElseExpr(FalseConst, _, right) => right
    case e => e
  }

  def simplifyLoop(loop: WhileInstr): Node = WhileInstr(simplify(loop.cond), simplify(loop.body)) match {
    case WhileInstr(FalseConst, body) => NodeList(Nil)
    case w => w
  }
}
